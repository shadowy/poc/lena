import { CompanyService } from '@app/company/company/company.service';
import { Company } from '@app/company/db';
import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';

@Crud({
  model: {
    type: Company
  }
})
@Controller('company')
export class CompanyController implements CrudController<Company>{
  constructor(public service: CompanyService) {}
}
