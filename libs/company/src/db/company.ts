import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Company {
  @PrimaryGeneratedColumn() @ApiProperty() id: number;
  @Column() @ApiProperty() name: string;
  @Column() @ApiProperty() description: string;
  @Column() @ApiProperty() isActive: boolean;
}
