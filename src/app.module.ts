import { CompanyModule } from '@app/company';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppService } from './app.service';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'test',
    password: 'test',
    database: 'test',
    synchronize: true,
    autoLoadEntities: true,
  }), CompanyModule],
  controllers: [],
  providers: [AppService],
  exports: [TypeOrmModule]
})
export class AppModule {}
